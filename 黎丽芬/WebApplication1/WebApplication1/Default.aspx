﻿<%@ Page Title="Home Page" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Default.aspx.cs" Inherits="WebApplication1._Default" %>

<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">

    <style >
        th{
            text-align :center;
        }
    </style>

    <asp:GridView runat ="server" ID ="Gv1" OnRowEditing="Gv1_RowEditing" OnRowCancelingEdit="Gv1_RowCancelingEdit" OnRowUpdating="Gv1_RowUpdating" OnRowDeleting="Gv1_RowDeleting" AutoGenerateColumns="False" BackColor="White" BorderColor="#CC9966" BorderStyle="None" BorderWidth="1px" CellPadding="4" Height="452px" Width="1158px">

        <Columns >

            <asp:BoundField HeaderText="id" DataField="Id" ReadOnly="true"  />
            <asp:BoundField HeaderText="老虎" DataField ="Title" />
            <asp:BoundField HeaderText="内容" DataField ="Content" />
            <asp:BoundField HeaderText="作者" DataField ="Author" />
            <asp:BoundField HeaderText="IsActived" DataField ="IsActived" />
            <asp:BoundField HeaderText ="IsDelted" DataField ="IsDelted" />
            <asp:BoundField HeaderText="创建时间" DataField="CreatedTime" />
            <asp:BoundField HeaderText="更新时间" DataField ="UpdatedTime" />
            <asp:BoundField HeaderText="备注" DataField="Remarks" />

            <asp:CommandField HeaderText="操作" ShowDeleteButton="true" ShowEditButton="true" ShowHeader="true" ButtonType="Button" />
             
        </Columns>

        <FooterStyle BackColor="#FFFFCC" ForeColor="#330099" />
        <HeaderStyle BackColor="#990000" Font-Bold="True" ForeColor="#FFFFCC" />
        <PagerStyle BackColor="#FFFFCC" ForeColor="#330099" HorizontalAlign="Center" />

        <RowStyle HorizontalAlign ="Center" BackColor="White" ForeColor="#330099" />

        <SelectedRowStyle BackColor="#FFCC66" Font-Bold="True" ForeColor="#663399" />
        <SortedAscendingCellStyle BackColor="#FEFCEB" />
        <SortedAscendingHeaderStyle BackColor="#AF0101" />
        <SortedDescendingCellStyle BackColor="#F6F0C0" />
        <SortedDescendingHeaderStyle BackColor="#7E0000" />

    </asp:GridView>

    <asp:Button Text ="添加" runat ="server" OnClick="Unnamed_Click" Height="37px" Width="67px" />

</asp:Content>
