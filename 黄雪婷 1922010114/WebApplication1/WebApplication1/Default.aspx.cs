﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;

namespace WebApplication1
{
    public partial class _Default : Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                FillData();
            }
        }
        private void FillData()
        {
            var sql = "Select * from Articles";
            var dt = DbHelper.GetDataTable(sql);

            GridView1.DataSource = dt;
            GridView1.DataBind();
        }

        protected void GridView1_RowEditing(object sender, GridViewEditEventArgs e)
        {
            GridView1.EditIndex = e.NewEditIndex;
            FillData();
        }

        protected void GridView1_RowCancelingEdit(object sender, GridViewCancelEditEventArgs e)
        {
            GridView1.EditIndex = -1;
            FillData();
        }
        private string c(int rowindex, int cellindex)
        {
            var z = GridView1.Rows[rowindex].Cells[cellindex];
            var c = z.Controls.Count > 0 ? ((TextBox)z.Controls[0]).Text : z.Text;

            return c;
        }
        protected void GridView1_RowDeleting(object sender, GridViewDeleteEventArgs e)
        {
            var id = c(e.RowIndex, 0);
            var sql = string.Format("delete from Articles where id={0}", id);

            DbHelper.AddOrUpdateOrDelete(sql);
            GridView1.EditIndex = -1;
            FillData();
        }

        protected void GridView1_RowUpdating(object sender, GridViewUpdateEventArgs e)
        {
            var Id = c(e.RowIndex, 0).Replace("&nbsp;", "");
            var Title = c(e.RowIndex, 1);
            var Content = c(e.RowIndex, 2);
            var Author = c(e.RowIndex, 3);

            if (string.IsNullOrEmpty(Id))
            {
                var sql = string.Format("insert into Articles(Title,Content,Author) values('{0}','{1}','{2}')", Title, Content, Author);
                DbHelper.AddOrUpdateOrDelete(sql);
            }
            else
            {
                var sql = string.Format("update Articles set Title='{0}',Content='{1}',Author='{2}'", Title, Content, Author, Id);
                DbHelper.AddOrUpdateOrDelete(sql);
            }
            GridView1.EditIndex = -1;
            FillData();
        }

        protected void Dd_Click(object sender, EventArgs e)
        {
            DataTable dt = new DataTable();
            dt.Columns.Add(new DataColumn("Id"));
            dt.Columns.Add(new DataColumn("Title"));
            dt.Columns.Add(new DataColumn("Content"));
            dt.Columns.Add(new DataColumn("Author"));

            foreach (GridViewRow row in GridView1.Rows)
            {
                var tempRow = dt.NewRow();
                tempRow["Id"] = c(row.RowIndex, 0);
                tempRow["Title"] = c(row.RowIndex, 1);
                tempRow["Content"] = c(row.RowIndex, 2);
                tempRow["Author"] = c(row.RowIndex, 3);

                dt.Rows.Add(tempRow);
            }
            var temp = dt.NewRow();
            dt.Rows.Add(temp);
            GridView1.DataSource = dt;
            GridView1.DataBind();
            GridView1.EditIndex = dt.Rows.Count - 1;
            GridView1.DataSource = dt;
            GridView1.DataBind();
        }
    }
}