﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;

namespace WebApplication1
{
    public partial class _Default : Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                filldata();
            }
        }
        private void filldata()
        {
            var sql = "select * from Articles ";
            var dt = DbHelper.GetDataTable(sql);

            GridView1.DataSource = dt;
            GridView1.DataBind();
        }

        protected void GridView1_RowEditing(object sender, GridViewEditEventArgs e)
        {
            GridView1.EditIndex = e.NewEditIndex;
            filldata();
        }

        protected void GridView1_RowCancelingEdit(object sender, GridViewCancelEditEventArgs e)
        {
            GridView1.EditIndex = -1;
            filldata();
        }


        private string aaa(int rowsindex, int cellsindex)
        {
            var bbb = GridView1.Rows[rowsindex].Cells[cellsindex];
            var ccc = bbb.Controls.Count > 0 ? ((TextBox)bbb.Controls[0]).Text : bbb.Text;
            return ccc;
        }
        protected void GridView1_RowUpdating(object sender, GridViewUpdateEventArgs e)
        {
            var id = aaa(e.RowIndex, 0).Replace("&nbsp;", "");
            var title = aaa(e.RowIndex, 1);
            var content = aaa(e.RowIndex, 2);
            var author = aaa(e.RowIndex, 3);

            if (string.IsNullOrEmpty(id))
            {
                var sql = string.Format("insert into Articles (Title,Content,Author) values('{0}','{1}','{2}')", title, content, author);
                DbHelper.AddOrUpdateOrDelete(sql);
            }
            else
            {
                var sql = string.Format("update Articles set Title='{0}',Content='{1}',Author='{2}'  where Id='{3}'", title,content,author,id);
                DbHelper.AddOrUpdateOrDelete(sql);
            }

            GridView1.EditIndex = -1;
            filldata();
        }

        protected void GridView1_RowDeleting(object sender, GridViewDeleteEventArgs e)
        {
            var id = aaa(e.RowIndex, 0).Replace("&nbsp;", "");
            var sql = string.Format("delete from Articles where Id='{0}'", id);
            DbHelper.AddOrUpdateOrDelete(sql);
            GridView1.EditIndex = -1;
            filldata();
        }

        protected void Unnamed_Click(object sender, EventArgs e)
        {
            DataTable dt = new DataTable();
            dt.Columns.Add(new DataColumn("Id"));
            dt.Columns.Add(new DataColumn("Title"));
            dt.Columns.Add(new DataColumn("Content"));
            dt.Columns.Add(new DataColumn("Author"));

            foreach(GridViewRow row in GridView1.Rows)
            {
                var ddd = dt.NewRow();
                ddd["Id"] = aaa(row.RowIndex, 0);
                ddd["Title"] = aaa(row.RowIndex, 1);
                ddd["Content"] = aaa(row.RowIndex, 2);
                ddd["Author"] = aaa(row.RowIndex, 3);

                dt.Rows.Add(ddd);
            }

            var eee = dt.NewRow();
            dt.Rows.Add(eee);
            GridView1.DataSource = dt;
            GridView1.DataBind();
            GridView1.EditIndex = dt.Rows.Count - 1;
            GridView1.DataSource = dt;
            GridView1.DataBind();

        }
    }
}