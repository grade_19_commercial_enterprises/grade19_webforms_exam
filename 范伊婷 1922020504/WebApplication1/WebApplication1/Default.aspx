﻿<%@ Page Title="Home Page" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Default.aspx.cs" Inherits="WebApplication1._Default" %>

<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">
    <style>

        th{
            text-align:center;
            width:80px;
            color:blue;
        }
        td{
            text-align:center;
        }
    </style>
  
    <asp:Button runat="server" Text="添加" OnClick="Unnamed_Click"  BackColor="#ccffff"/>
    <asp:GridView ID="GridView1" runat="server" AutoGenerateColumns="false" OnRowCancelingEdit="GridView1_RowCancelingEdit" OnRowEditing="GridView1_RowEditing" OnRowUpdating="GridView1_RowUpdating" OnRowDeleting="GridView1_RowDeleting">
        <Columns>
            <asp:BoundField DataField="Id" HeaderText="Id" ReadOnly="true"  HeaderStyle-BackColor="#ccffff" ItemStyle-BackColor="#ccffff"/>
            <asp:BoundField DataField ="Title"  HeaderText="标题"  HeaderStyle-BackColor="#ccffff" ItemStyle-BackColor="#ccffff"/>
             <asp:BoundField DataField ="Content"  HeaderText="内容"  HeaderStyle-BackColor="#ccffff" ItemStyle-BackColor="#ccffff"/>
            <asp:BoundField DataField ="Author"  HeaderText="作者"  HeaderStyle-BackColor="#ccffff" ItemStyle-BackColor="#ccffff"/>
            <asp:CommandField HeaderText="操作" ShowDeleteButton="True" ShowEditButton="True" ShowHeader="True"  HeaderStyle-BackColor="#ccffff" ItemStyle-BackColor="#ffffcc"/>
        </Columns>
    </asp:GridView>

  

</asp:Content>
