﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace WebApplication1
{
    public partial class _Default : Page
    {
        private void FillData()
        {
            var sql = "select * from Articles";
            var dt = DbHelper.GetDataTable(sql);
            GridView1.DataSource = dt;
            GridView1.DataBind();
        }
        private string Getvalue(int rowIndex, int colIndex)
        {
            var control = GridView1.Rows[rowIndex].Cells[colIndex];
            var res = control.Controls.Count > 0 ? ((TextBox)control.Controls[0]).Text : control.Text;
            return res;
        }
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                FillData();
            }
        }

        protected void GridView1_RowDeleting(object sender, GridViewDeleteEventArgs e)
        {
            var Id =int.Parse( Getvalue(e.RowIndex, 0));
            var sql = string.Format("delete from  Articles where Id={0}",Id);
            DbHelper.AddOrUpdateOrDelete(sql);
            GridView1.EditIndex = -1;
            FillData();
        }

        protected void GridView1_RowEditing(object sender, GridViewEditEventArgs e)
        {
            GridView1.EditIndex = e.NewEditIndex;
            FillData();
        }

        protected void GridView1_RowUpdating(object sender, GridViewUpdateEventArgs e)
        {
            var Id = Getvalue(e.RowIndex, 0).Replace("nbsp;", "");
            var Title = Getvalue(e.RowIndex, 1);
            var Conten = Getvalue(e.RowIndex, 2);
            var Author = Getvalue(e.RowIndex, 3);
            if (string.IsNullOrEmpty(Id))
            {
                var sql = string.Format("insert into Articles (Title,Content,Author) values ('{0}','{1}','{2}')", Title, Conten, Author);
                DbHelper.AddOrUpdateOrDelete(sql);
            }
            else
            {
                var sql = string.Format("update  Articles set Title='{0}',Content='{1}',Author='{2}' where Id={3}", Title, Conten, Author,Id);
                DbHelper.AddOrUpdateOrDelete(sql);
            }
            GridView1.EditIndex = -1;
            FillData();

        }

        protected void GridView1_RowCancelingEdit(object sender, GridViewCancelEditEventArgs e)
        {
            GridView1.EditIndex = -1;
            FillData();
        }

        protected void Unnamed_Click(object sender, EventArgs e)
        {
            DataTable dt = new DataTable();
            dt.Columns.Add(new DataColumn("Id"));
            dt.Columns.Add(new DataColumn("Title"));
            dt.Columns.Add(new DataColumn("Content"));
            dt.Columns.Add(new DataColumn("Author"));
            foreach (GridViewRow row in GridView1.Rows)
            {
                var temp = dt.NewRow();
                temp["Id"] = Getvalue(row.RowIndex, 0);
                temp["Title"] = Getvalue(row.RowIndex, 1);
                temp["Content"] = Getvalue(row.RowIndex, 2);
                temp["Author"] = Getvalue(row.RowIndex, 3);
                dt.Rows.Add(temp);

            }
            var tem = dt.NewRow();
            dt.Rows.Add(tem);
            GridView1.DataSource = dt;
            GridView1.DataBind();
            GridView1.EditIndex = dt.Rows.Count - 1;
            GridView1.DataSource = dt;
            GridView1.DataBind();
        }
    }
}