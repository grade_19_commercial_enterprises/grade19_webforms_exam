﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace WebApplication1
{
    public partial class _Default : Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                FillData();
            }
        }

        private void FillData()
        {
            var sql = string.Format("select * from Articles");
            var dt = DBHelper.GetDataTable(sql);
            GridView1.DataSource = dt;
            GridView1.DataBind();
        }
        private string GetValues(int rowIndex,int colIndex)
        {
            var control = GridView1.Rows[rowIndex].Cells[colIndex];
            var txt = control.Controls.Count > 0 ? ((TextBox)control.Controls[0]).Text : control.Text;
            return txt;
        }

        protected void GridView1_RowEditing(object sender, GridViewEditEventArgs e)
        {
            GridView1.EditIndex = e.NewEditIndex;
            FillData();
        }

        protected void GridView1_RowCancelingEdit(object sender, GridViewCancelEditEventArgs e)
        {
            GridView1.EditIndex = -1;
            FillData();
        }

        protected void GridView1_RowUpdating(object sender, GridViewUpdateEventArgs e)
        {
            var id = GetValues(e.RowIndex, 0).Replace("&nbsp;", "");
            var title = GetValues(e.RowIndex, 1);
            var content = GetValues(e.RowIndex, 2);
            var author = GetValues(e.RowIndex, 3);
            if (string.IsNullOrEmpty(id))
            {
                var sql = string.Format("insert into Articles (Title,Content,Author) values('{0}','{1}','{2}')",title,content,author);
                DBHelper.AddOrUpdateOrDelete(sql);
            }
            else
            {
                var sql = string.Format("update Articles set Title='{0}',Content='{1}',Author='{2}' where Id='{3}'", title, content, author, id);
                DBHelper.AddOrUpdateOrDelete(sql);
            }
            GridView1.EditIndex = -1;
            FillData();
        }

        protected void GridView1_RowDeleting(object sender, GridViewDeleteEventArgs e)
        {
            var id = GetValues(e.RowIndex, 0).Replace("&nbsp;", "");
            var sql = string.Format("delete from Articles where Id='{0}'", id);
            DBHelper.AddOrUpdateOrDelete(sql);
            GridView1.EditIndex = -1;
            FillData();
        }

        protected void btnAdd_Click(object sender, EventArgs e)
        {
            DataTable dt = new DataTable();
            dt.Columns.Add(new DataColumn("Id"));
            dt.Columns.Add(new DataColumn("Title"));
            dt.Columns.Add(new DataColumn("Content"));
            dt.Columns.Add(new DataColumn("Author"));
            foreach(GridViewRow row in GridView1.Rows)
            {
                var tempVlaue = dt.NewRow();
                tempVlaue["Id"] = GetValues(row.RowIndex, 0).Replace("&nbsp;", "");
                tempVlaue["Title"] = GetValues(row.RowIndex, 1);
                tempVlaue["Content"] = GetValues(row.RowIndex, 2);
                tempVlaue["Author"] = GetValues(row.RowIndex, 3);
                dt.Rows.Add(tempVlaue);
            }
            var tempItem = dt.NewRow();
            dt.Rows.Add(tempItem);

            GridView1.DataSource = dt;
            GridView1.DataBind();

            GridView1.EditIndex = dt.Rows.Count - 1;

            GridView1.DataSource = dt;
            GridView1.DataBind();
        }
    }
}