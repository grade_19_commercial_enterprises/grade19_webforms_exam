﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Data;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace WebApplication1
{
    public partial class _Default : Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                FillData();
            }
        }
        private void FillData()
        {
            var sql = "select * from Articles";
            var dt = DbHelper.GetDataTable(sql);

            gv1.DataSource = dt;
            gv1.DataBind();
        }
        private string GetValueWithIndexClomnsIndex(int rowIndex, int colIndex)
        {
            var cout= gv1.Rows[rowIndex].Cells[colIndex];
            var GetValueWithIndexClomnsIndex = cout.Controls.Count > 0 ? ((TextBox)cout.Controls[0]).Text : cout.Text;

            return GetValueWithIndexClomnsIndex;
        }
       
        protected void Unnamed_Click(object sender, EventArgs e)
        {
            DataTable dt = new DataTable();
            dt.Columns.Add(new DataColumn("Id"));
            dt.Columns.Add(new DataColumn("Title"));
            dt.Columns.Add(new DataColumn("Content"));
            dt.Columns.Add(new DataColumn("Author"));

            foreach (GridViewRow row in gv1.Rows)
            {
                var tempRow = dt.NewRow();
                tempRow["Id"] = GetValueWithIndexClomnsIndex(row.RowIndex, 0);
                tempRow["Title"] = GetValueWithIndexClomnsIndex(row.RowIndex, 1);
                tempRow["Content"] = GetValueWithIndexClomnsIndex(row.RowIndex, 2);
                tempRow["Author"] = GetValueWithIndexClomnsIndex(row.RowIndex, 2);

                dt.Rows.Add(tempRow);
            }
            var temp = dt.NewRow();
            dt.Rows.Add(temp);
            gv1.DataSource = dt;
            gv1.DataBind();
            gv1.EditIndex = dt.Rows.Count - 1;
            gv1.DataSource = dt;
            gv1.DataBind();
        }

        protected void gv1_RowCancelingEdit(object sender, GridViewCancelEditEventArgs e)
        {
            gv1.EditIndex= - 1;
            FillData();
        }

        protected void gv1_RowDeleting(object sender, GridViewDeleteEventArgs e)
        {
            var id = GetValueWithIndexClomnsIndex(e.RowIndex, 0);
            var sql = string.Format("delete from Articles where id={0}", id);

            DbHelper.AddOrUpdateOrDelete(sql);
            gv1.EditIndex = -1;
            FillData();
        }
        protected void gv1_RowEditing(object sender, GridViewEditEventArgs e)
        {
            gv1.EditIndex=e.NewEditIndex;
            FillData();
        }

        protected void gv1_RowUpdating(object sender, GridViewUpdateEventArgs e)
        {
            var Id = GetValueWithIndexClomnsIndex(e.RowIndex, 0).Replace("&nbsp;","");
            var Title = GetValueWithIndexClomnsIndex(e.RowIndex, 1);
            var Content = GetValueWithIndexClomnsIndex(e.RowIndex, 2);
            var Author = GetValueWithIndexClomnsIndex(e.RowIndex, 3);

            if (string.IsNullOrEmpty(Id))
            {
                var sql = string.Format("insert into Articles(Title,Content,Author) values('{0}','{1}','{2}')", Title, Content, Author);
                DbHelper.AddOrUpdateOrDelete(sql);
            }
            else
            {
                var sql = string.Format("update Articles set Title='{0}',Content='{1}',Author='{2}'", Title, Content, Author, Id);
                DbHelper.AddOrUpdateOrDelete(sql);
            }
            gv1.EditIndex = -1;
            FillData();
        }
    }
}