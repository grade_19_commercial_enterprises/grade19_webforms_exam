﻿<%@ Page Title="Home Page" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Default.aspx.cs" Inherits="WebApplication1._Default" %>

<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">
   
 <asp:Button  runat="server" Text="添加" OnClick="Unnamed_Click" />
     <style>
        th{
            text-align:center;
            width:80px;
        }
        tr{
            text-align:center;
        }
    </style>
 <asp:GridView ID="gv1" runat="server" AutoGenerateColumns="False" OnRowCancelingEdit="gv1_RowCancelingEdit" OnRowDeleting="gv1_RowDeleting" OnRowEditing="gv1_RowEditing" OnRowUpdating="gv1_RowUpdating" BackColor="White" BorderColor="#CCCCCC" BorderStyle="None" BorderWidth="1px" CellPadding="4" ForeColor="Black" GridLines="Horizontal">
     <Columns>
         <asp:BoundField DataField="Id" HeaderText="Id" ReadOnly="true" />
         <asp:BoundField DataField="Content" HeaderText="内容" />
         <asp:BoundField DataField="Author" HeaderText="作者" />
         <asp:CommandField ShowCancelButton="true" ShowEditButton="true" ShowDeleteButton="true" HeaderText="操作" />
     </Columns>
     <FooterStyle BackColor="#CCCC99" ForeColor="Black" />
     <HeaderStyle BackColor="#333333" Font-Bold="True" ForeColor="White" />
     <PagerStyle BackColor="White" ForeColor="Black" HorizontalAlign="Right" />
     <SelectedRowStyle BackColor="#CC3333" Font-Bold="True" ForeColor="White" />
     <SortedAscendingCellStyle BackColor="#F7F7F7" />
     <SortedAscendingHeaderStyle BackColor="#4B4B4B" />
     <SortedDescendingCellStyle BackColor="#E5E5E5" />
     <SortedDescendingHeaderStyle BackColor="#242121" />
 </asp:GridView>
</asp:Content>
