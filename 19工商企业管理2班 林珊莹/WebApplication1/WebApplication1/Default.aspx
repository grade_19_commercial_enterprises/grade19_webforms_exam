﻿<%@ Page Title="Home Page" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Default.aspx.cs" Inherits="WebApplication1._Default" %>

<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">

    <style>
        th{
            text-align:center;
            width:100px;
            background-color:burlywood;
            color:brown;
        }
        tr{
            text-align:center;
            width:100px;
            background-color:bisque;
            color:brown;
            height:70px;
        }
        body{
            width:400px;
            height:1000px;
            margin:0px auto;
        }
    </style>
    <asp:Button runat="server" Text="新增" OnClick="Unnamed_Click" />
    <asp:GridView runat="server" AutoGenerateColumns="false" ID="GridView1" OnRowCancelingEdit="GridView1_RowCancelingEdit" OnRowDeleting="GridView1_RowDeleting" OnRowEditing="GridView1_RowEditing" OnRowUpdating="GridView1_RowUpdating">
        <Columns>
            <asp:BoundField DataField="Id" HeaderText="Id" ReadOnly="true" />
            <asp:BoundField DataField="Title" HeaderText="标题" />
            <asp:BoundField DataField="Content" HeaderText="内容" />
            <asp:BoundField DataField="Author" HeaderText="作者" />
            <asp:CommandField ButtonType="Button" HeaderText="操作" ShowDeleteButton="true" ShowEditButton="true" />
        </Columns>
    </asp:GridView>

</asp:Content>
