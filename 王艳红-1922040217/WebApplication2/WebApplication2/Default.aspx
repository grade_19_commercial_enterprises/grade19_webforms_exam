﻿<%@ Page Title="Home Page" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Default.aspx.cs" Inherits="WebApplication2._Default" %>

<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">

     <style>
         th{ width:100px;
             text-align:center;
             background-color:darkgray;

         }
         td{
             background-color:azure;
         }

         body{

             background-color:lightblue;
         }

     </style>
    <asp:Button runat="server" Text="新增"  OnClick="Unnamed_Click" BackColor="darkgray" />
    <asp:GridView ID="GridView1" runat="server" OnRowCancelingEdit="GridView1_RowCancelingEdit" OnRowDeleting="GridView1_RowDeleting" OnRowEditing="GridView1_RowEditing" OnRowUpdating="GridView1_RowUpdating" AutoGenerateColumns="false">
        <Columns>
            <asp:BoundField DataField="Id" HeaderText="Id" ReadOnly="true" />
           <asp:BoundField DataField="Title" HeaderText="标题" />
           <asp:BoundField DataField="Content" HeaderText="内容" />
            <asp:BoundField DataField="Author" HeaderText="作者"  />
            <asp:CommandField HeaderText="操作" ShowDeleteButton="True" ShowEditButton="True" />
        </Columns>
    </asp:GridView>

</asp:Content>
