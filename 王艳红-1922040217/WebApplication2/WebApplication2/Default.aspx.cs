﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace WebApplication2
{
    public partial class _Default : Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {

                Filldata();
            }


        }

        protected void GridView1_RowCancelingEdit(object sender, GridViewCancelEditEventArgs e)
        {
            GridView1.EditIndex = -1;
            Filldata();
        }

        protected void GridView1_RowDeleting(object sender, GridViewDeleteEventArgs e)
        {
            var id = int.Parse(getvalue(e.RowIndex, 0));

            var sql = string.Format("delete from Articles where id={0}", id);
            dbhelper.AddOrUpdateOrDelete(sql);

            GridView1.EditIndex = -1;
            Filldata();
        }

        protected void GridView1_RowEditing(object sender, GridViewEditEventArgs e)
        {
            GridView1.EditIndex = e.NewEditIndex;
            Filldata();
        }

        protected void GridView1_RowUpdating(object sender, GridViewUpdateEventArgs e)
        {
            var tempValue = getvalue(e.RowIndex, 0);

            var Title = getvalue(e.RowIndex, 1);
            var Content = getvalue(e.RowIndex, 2);
            var author = getvalue(e.RowIndex, 3);

            var isok = int.TryParse(tempValue, out int id);

            if (isok)
            {
                var sql = string.Format("update Articles set Title='{0}',Content='{1}',Author='{2}' where id={3}", Title, Content, author, id);
                dbhelper.AddOrUpdateOrDelete(sql);
            }
            else
            {
                var sql = string.Format("insert into Articles(Title,Content, Author,) values('{0}','{1}','{2}')", Title, Content, author);
                dbhelper.AddOrUpdateOrDelete(sql);
            }
            GridView1.EditIndex = -1;
            Filldata();

        }

        protected void Unnamed_Click(object sender, EventArgs e)
        {
            var dt = clone();
            GridView1.DataSource = dt;
            GridView1.DataBind();
            GridView1.EditIndex = dt.Rows.Count - 1;
            GridView1.DataSource = dt;
            GridView1.DataBind();
        }
        private void Filldata()
        {

            var sql = "select * from Articles ";
            var dt = dbhelper.GetDataTable(sql);
            GridView1.DataSource = dt;
            GridView1.DataBind();


        }
        private string getvalue(int RowIndex, int Colindex)
        {
            var Control = GridView1.Rows[RowIndex].Cells[Colindex];
            var txt = Control.Controls.Count > 0 ? ((TextBox)Control.Controls[0]).Text : Control.Text;
            return txt;



        }
        private DataTable clone()
        {
            DataTable dt = new DataTable();
            dt.Columns.Add(new DataColumn("Id"));
            dt.Columns.Add(new DataColumn("Title"));
            dt.Columns.Add(new DataColumn("Content"));
            dt.Columns.Add(new DataColumn("Author"));



      
            foreach (GridViewRow row in GridView1.Rows)
            {
                var tempRow = dt.NewRow();
                tempRow["Id"] = getvalue(row.RowIndex, 0);
                tempRow["Title"] = getvalue(row.RowIndex, 1);
                tempRow["Content"] = getvalue(row.RowIndex, 2);
                tempRow["Author"] = getvalue(row.RowIndex, 3);

                dt.Rows.Add(tempRow);
            }
            dt.Rows.Add(dt.NewRow());
            return dt;




        }

    }
}