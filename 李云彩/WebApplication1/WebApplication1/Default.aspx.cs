﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using WebApplication1;

namespace WebApplication1
{
    public partial class _Default : Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (! IsPostBack)
            {
                FillData();
            }
        }

        protected void Unnamed_Click(object sender, EventArgs e)
        {
            var tem = GetValuesFromGridView();
            gvl.DataSource = tem;
            gvl.DataBind();
            gvl.EditIndex = tem.Rows.Count - 1;
      
        }
        private void FillData()
        {
            var sql = string.Format("select * from Articles");
            var dt = DbHelper.GetDataTable(sql);
            gvl.DataSource = dt;
            gvl.DataBind();
        }
        private string GetValues(int rowIndex, int colIndex)
        {
            var temper = gvl.Rows[rowIndex].Cells[colIndex];
            var tex = temper.Controls.Count > 0 ? ((TextBox)temper.Controls[0]).Text : temper.Text;
            return tex;
        }

        protected void gvl_RowCancelingEdit(object sender, GridViewCancelEditEventArgs e)
        {
            gvl.EditIndex = -1;
            FillData();
        }
        protected void gvl_RowDeleting(object sender, GridViewDeleteEventArgs e)
        {
            var id = int.Parse(GetValues(e.RowIndex, 0));
            var sql = string.Format("delete from Articles where Id = {0} ", id);
            var dt = DbHelper.GetDataTable(sql);
            gvl.DataSource = dt;
            gvl.EditIndex = -1;
            FillData();
        }

        protected void gvl_RowEditing(object sender, GridViewEditEventArgs e)
        {
            gvl.EditIndex = e.NewEditIndex;
            FillData();
        }
        private DataTable GetValuesFromGridView()
        {
            DataTable table = new DataTable();
            table.Columns.Add(new DataColumn("Id"));
            table.Columns.Add(new DataColumn("Title"));
            table.Columns.Add(new DataColumn("Content"));
            table.Columns.Add(new DataColumn("Author"));
            foreach (GridViewRow row in gvl.Rows)
            {
                var temper = table.NewRow();
                temper["Id"] = GetValues(row.RowIndex, 0);
                temper["Title"] = GetValues(row.RowIndex, 1);
                temper["Content"] = GetValues(row.RowIndex, 2);
                temper["Author"] = GetValues(row.RowIndex, 3);
                table.Rows.Add(temper);
            }
            table.Rows.Add(table.NewRow());
            FillData();
            return table;

        }
        
        protected void gvl_RowUpdating(object sender, GridViewUpdateEventArgs e)
        {
                var temper = GetValues(e.RowIndex, 0);
                var isok = int.TryParse(temper, out int id);
                var Title = GetValues(e.RowIndex, 1);
                var Content = GetValues(e.RowIndex, 2);
                var Author = GetValues(e.RowIndex, 3);
            if (isok)
                {
                    var sql = string.Format("update Articles set Title = '{0}', Content = '{1}' where Id = {2}",Title ,Content,id ,Author);
                    DbHelper.AddOrUpdateOrDelete(sql);
                }
                else
                {
                    var sql = string.Format("insert into Articles (Title,Content,Author) Values ('{0}','{1}','{2}') ", Title ,Content,Author );
                    DbHelper.AddOrUpdateOrDelete(sql);
                }
                gvl.EditIndex = -1;
                FillData();
            }
        }


    }
