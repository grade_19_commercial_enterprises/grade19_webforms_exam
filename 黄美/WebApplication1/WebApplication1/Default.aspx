﻿<%@ Page Title="Home Page" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Default.aspx.cs" Inherits="WebApplication1._Default" %>

<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">
    <asp:Button runat="server" Text="添加" OnClick="Unnamed_Click" />
 <style>
     th{
         text-align:center;
         width:80px;
         color:antiquewhite;

     }
     td{
         text-align:center;

     }
     div{
         text-align:center;
     }
 </style>   

    <asp:GridView Style="margin:0px auto" ID="gv1" runat="server" AutoGenerateColumns="False" OnRowDeleting="gv1_RowDeleting"  OnRowEditing="gv1_RowEditing" OnRowUpdating="gv1_RowUpdating"  OnRowCancelingEdit="gv1_RowCancelingEdit" >
        <Columns>
          <asp:BoundField DataField="id" ReadOnly="true" HeaderText="id" />
          <asp:BoundField DataField="Title" HeaderText="标题"   />
          <asp:BoundField DataField="Content" HeaderText="内容" />
           <asp:BoundField DataField="Author" HeaderText="作者" />
          <asp:CommandField ShowDeleteButton="true" ShowEditButton="true" HeaderText="操作" />

      </Columns>
  </asp:GridView>
</asp:Content>
