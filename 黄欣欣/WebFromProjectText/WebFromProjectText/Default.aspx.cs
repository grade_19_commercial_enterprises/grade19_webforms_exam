﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using WebApplication1;

namespace WebFromProjectText
{
    public partial class _Default : Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                DataFill();
            }
        }
        private void DataFill()
        {
            var sql = "select * from Articles";
            var dt = DbHelper.GetDataTable(sql);
            gv1.DataSource = dt;
            gv1.DataBind();
        }

        protected void gv1_RowCancelingEdit(object sender, GridViewCancelEditEventArgs e)
        {
            gv1.EditIndex = -1;
            DataFill();

        }
        private string GetValue(int rowIndex, int colIndex)
        {

            var cont = gv1.Rows[rowIndex].Cells[colIndex];
            var value = cont.Controls.Count > 0 ? ((TextBox)cont.Controls[0]).Text : cont.Text;
            return value;
        }
       


        protected void gv1_RowDeleting(object sender, GridViewDeleteEventArgs e)
        {
            var id = int.Parse(GetValue(e.RowIndex, 0));
            var sql = string.Format("delect Articles where id ={0}", id);
            var dt = DbHelper.GetDataTable(sql);
            gv1.EditIndex = -1;
            DataFill();
        }

        protected void gv1_RowEditing(object sender, GridViewEditEventArgs e)
        {
            gv1.EditIndex = e.NewEditIndex;
            DataFill();
        }

        protected void gv1_RowUpdating(object sender, GridViewUpdateEventArgs e)
        {
            var Title = GetValue(e.RowIndex, 1);
            var Content = GetValue(e.RowIndex, 2);
            var id = GetValue(e.RowIndex, 0);
            var Author = GetValue(e.RowIndex, 3);



            var sql = string.Format("upate Articles  set  title = '{0}'Content = '{1}',Author = '{2}' where id ={3}", Title, Content, Author, id);
            DbHelper.AddOrUpadteOrDel(sql);

         }
        
     
     
       
        
          private DataTable CloneDataFromGv1()
            {
                DataTable dt = new DataTable();
                dt.Columns.Add(new DataColumn("Id"));

                dt.Columns.Add(new DataColumn("Title"));
                dt.Columns.Add(new DataColumn("Content"));
                dt.Columns.Add(new DataColumn("Author"));
                foreach (GridViewRow row in gv1.Rows)
            {
                var tempValue = dt.NewRow();
                tempValue["Id"] = GetValue(row.RowIndex, 0);
                tempValue["Title"] = GetValue(row.RowIndex, 1);
                tempValue["Content"] = GetValue(row.RowIndex, 2);
                tempValue["Author"] = GetValue(row.RowIndex, 3);

                dt.Rows.Add(tempValue);
            }
            dt.Rows.Add(dt.NewRow());
            return dt;
        }

        protected void Unnamed_Click(object sender, EventArgs e)
        {
            DataTable dt = new DataTable();
            gv1.DataSource = dt;
            gv1.DataBind();
            gv1.EditIndex = -1;
            DataFill();
            gv1.DataSource = dt;
            gv1.DataBind();
        }
    }
}