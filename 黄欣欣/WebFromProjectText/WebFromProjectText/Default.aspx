﻿<%@ Page Title="Home Page" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Default.aspx.cs" Inherits="WebFromProjectText._Default" %>

<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">
    <style>
        th{
            text-align:center;
            width:80px;
            color:lightsalmon;
        }
        td{
            text-align:center;
            background-color:navajowhite;

        }
    </style>
    <asp:Button  runat ="server" Text ="添加" OnClick ="Unnamed_Click"/>

   <asp:GridView ID ="gv1" runat ="server" AutoGenerateColumns ="false" OnRowCancelingEdit ="gv1_RowCancelingEdit"
        OnRowDeleting ="gv1_RowDeleting" OnRowEditing ="gv1_RowEditing" OnRowUpdating ="gv1_RowUpdating">
       <Columns>
           <asp:BoundField  DataField ="Id" HeaderText ="Id" ReadOnly ="true"/>
           <asp:BoundField  DataField ="Title" HeaderText ="标题" />
           <asp:BoundField  DataField ="Content" HeaderText ="内容" />
           <asp:BoundField  DataField ="Author" HeaderText ="作者" />

           <asp:CommandField  ShowDeleteButton ="true" ShowEditButton ="true" ShowHeader ="true" HeaderText ="操作"/>
       </Columns>
   </asp:GridView>
</asp:Content>
