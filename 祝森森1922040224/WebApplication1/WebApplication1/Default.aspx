﻿<%@ Page Title="Home Page" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Default.aspx.cs" Inherits="WebApplication1._Default" %>

<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">
     <asp:Button Text ="添加" runat ="server" OnClick="Unnamed_Click" />
    <asp:GridView ID="GridView1" runat="server" AutoGenerateColumns ="false" OnRowEditing="GridView1_RowEditing" OnRowCancelingEdit="GridView1_RowCancelingEdit" 
        OnRowUpdating="GridView1_RowUpdating" OnRowDeleting="GridView1_RowDeleting">
        <Columns>
            <asp:BoundField  DataField ="Id" HeaderText ="Id"  ReadOnly ="true"/>
            <asp:BoundField  DataField  ="Title" HeaderText ="标题" />
            <asp:BoundField DataField ="Author" HeaderText ="作者" />
               <asp:BoundField  DataField  ="Content" HeaderText ="内容" />
            <asp:CommandField ButtonType="Button" HeaderText="操作" ShowDeleteButton="True" ShowEditButton="True" ShowHeader="True" />
        </Columns>
    </asp:GridView>

</asp:Content>
