﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace WebApplication1
{
    public partial class _Default : Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                FillData();
            }
        }

        private void FillData()
        {
            var sql = "select * from Articles";
            var dt = DbHelper.GetDataTable(sql);
            GridView1.DataSource = dt;
            GridView1.DataBind();
        }

        protected void GridView1_RowEditing(object sender, GridViewEditEventArgs e)
        {
            GridView1.EditIndex = e.NewEditIndex;
            FillData();
        }

        protected void GridView1_RowCancelingEdit(object sender, GridViewCancelEditEventArgs e)
        {
            GridView1.EditIndex = -1;
            FillData();
        }

        protected void GridView1_RowUpdating(object sender, GridViewUpdateEventArgs e)
        {
            var id =GetValu(e.RowIndex, 0).Replace("&nbsp;","");
            var Title = GetValu(e.RowIndex, 1);
            var Content = GetValu(e.RowIndex, 2);
            var Author = GetValu(e.RowIndex, 3);

            if (string.IsNullOrEmpty(id))
            {
                var sql = string.Format("insert into Articles (Title,Content,Author) values ('{0}','{1}','{2}')", Title, Content, id);
                DbHelper.AddOrUpdateOrDelete(sql);
            }
            else {
                var sql = string.Format("update Articles set Title='{0}',Content='{1}',Author='{2}' where id={3}", Title, Content, Author, id);
                DbHelper.AddOrUpdateOrDelete(sql);

            }

            GridView1.EditIndex = -1;
            FillData();
        }

        private string GetValu(int rowIndex, int colIndex)
        {
            var con = GridView1.Rows[rowIndex].Cells[colIndex ];
            var value = con.Controls.Count > 0 ? ((TextBox)con.Controls[0]).Text : con.Text;
            return value;
        }

        protected void GridView1_RowDeleting(object sender, GridViewDeleteEventArgs e)
        {
            var id = int.Parse(GetValu(e.RowIndex, 0));
            var sql = string.Format("delete  from  Articles where Id={0}", id);
            DbHelper.AddOrUpdateOrDelete(sql);
            FillData();
        }

        protected void Unnamed_Click(object sender, EventArgs e)
        {
            DataTable dt = new DataTable();
            dt.Columns.Add(new DataColumn("Id"));
            dt.Columns.Add(new DataColumn("Title")); 
            dt.Columns.Add(new DataColumn("Content"));
            dt.Columns.Add(new DataColumn("Author")); 

            foreach (GridViewRow row in GridView1.Rows)
            {
                var item = dt.NewRow();
                item["Id"] = GetValu(row.RowIndex, 0);
                item["Title"] = GetValu(row.RowIndex, 1);
                item["Content"] = GetValu(row.RowIndex, 2);
                item["Author"] = GetValu(row.RowIndex, 3);

                dt.Rows.Add(item);
            }
            var tempItem = dt.NewRow();
            dt.Rows.Add(tempItem);

            GridView1.DataSource = dt;
            GridView1.DataBind();

            GridView1.EditIndex =GridView1.Rows.Count -1;
           

            GridView1.DataSource = dt;
            GridView1.DataBind();

        }
    }
}