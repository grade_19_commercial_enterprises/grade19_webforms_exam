﻿<%@ Page Title="Home Page" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Default.aspx.cs" Inherits="WebApplication1._Default" %>

<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">
    <style>
        th{
            background-color:lightpink;
            text-align:center;
            width:20px;
        }
        td{
            background-color:lightpink;
        }
        body
        {
            margin:0px auto;
            background-color:pink;
            width:2600px;
        }
    </style>
    <asp:Button runat="server" Text="添加" OnClick="Unnamed_Click" />
    <asp:GridView ID="GridView1" runat="server" AutoGenerateColumns="false" OnRowCancelingEdit="GridView1_RowCancelingEdit"
         OnRowEditing="GridView1_RowEditing" OnRowDeleting="GridView1_RowDeleting" OnRowUpdating="GridView1_RowUpdating">
        <Columns>
            <asp:BoundField DataField="Id" HeaderText="Id" ReadOnly="true" />
             <asp:BoundField DataField="Title" HeaderText="题目"  />
             <asp:BoundField DataField="Content" HeaderText="数据" />
            <asp:BoundField DataField="Author" HeaderText="作者" />

            <asp:CommandField HeaderText="操作" ShowDeleteButton="True" ShowEditButton="True" ShowHeader="True" />
        </Columns>
    </asp:GridView>

</asp:Content>
