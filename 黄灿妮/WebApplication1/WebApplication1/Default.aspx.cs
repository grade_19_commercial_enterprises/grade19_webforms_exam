﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace WebApplication1
{
    public partial class _Default : Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                FillData();
            }
        }
        private void FillData()
        {
            var sql ="select * from Articles";
            var dt = DbHelper.GetDataTable(sql);
            GV1.DataSource = dt;
            GV1.DataBind();
        }
        private string GetValue(int rowIndex,int colIndex)
        {
            var cont = GV1.Rows[rowIndex].Cells[colIndex];
            var value = cont.Controls.Count > 0 ? ((TextBox)cont.Controls[0]).Text : cont.Text;
            return value;
        }
        private DataTable CloneDataFromGV1()
        {
            DataTable dt = new DataTable();
            dt.Columns.Add(new DataColumn("Id"));
            dt.Columns.Add(new DataColumn("Title"));
            dt.Columns.Add(new DataColumn("Content"));
            dt.Columns.Add(new DataColumn("Author"));
            foreach (GridViewRow row in GV1.Rows)
            {
                var tempRow = dt.NewRow();
                tempRow["Id"] = GetValue(row.RowIndex, 0);
                tempRow["Title"] = GetValue(row.RowIndex, 1);
                tempRow["Content"] = GetValue(row.RowIndex, 2);
                tempRow["Author"] = GetValue(row.RowIndex, 3);
                dt.Rows.Add(tempRow);
            }
            dt.Rows.Add(dt.NewRow());
            return dt;
        }
        protected void GV1_RowCancelingEdit(object sender, GridViewCancelEditEventArgs e)
        {
            GV1.EditIndex = -1;
            FillData();
        }

        protected void GV1_RowDeleting(object sender, GridViewDeleteEventArgs e)
        {
            var id = int.Parse(GetValue(e.RowIndex, 0));
            var sql = string.Format("delete Articles where id={0}", id);
            var dt = DbHelper.GetDataTable(sql);
            GV1.EditIndex = -1;
            FillData();
        }

        protected void GV1_RowEditing(object sender, GridViewEditEventArgs e)
        {
            GV1.EditIndex = e.NewEditIndex;
            FillData();
        }

        protected void GV1_RowUpdating(object sender, GridViewUpdateEventArgs e)
        {
            var Title = GetValue(e.RowIndex, 1);
            var Content = GetValue(e.RowIndex, 2);
            var Author = GetValue(e.RowIndex, 3);
            var tempVlaue = GetValue(e.RowIndex, 0);
            var isOK = int.TryParse(tempVlaue, out int id);
            if (isOK)
            {
                var sql = string.Format("update Articles set Title='{0}',Content='{1}',Author='{2}'where id={3}",Title,Content,Author,id); 
                DbHelper.AddOrUpdateOrDelete(sql);
            }
            else
            {
                var sql = string.Format("insert into Articles(Title,Content,Author)values('{0}','{1}','{2}')", Title, Content, Author);
                DbHelper.AddOrUpdateOrDelete(sql);
            }
            GV1.EditIndex = -1;
            FillData();
        }

        protected void Unnamed_Click(object sender, EventArgs e)
        {
            var dataTable = CloneDataFromGV1();
            GV1.DataSource = dataTable;
            GV1.DataBind();
            GV1.EditIndex = dataTable.Rows.Count - 1;
            GV1.DataSource = dataTable;
            GV1.DataBind();

        }
    }
}