﻿<%@ Page Title="Home Page" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Default.aspx.cs" Inherits="WebApplication1._Default" %>

<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">

    <style>
        th{
            text-align:center;
            width:80px;
            color:lightsalmon;
        }
        td{
            text-align:center;
            background-color:navajowhite;
        }
    </style>

    <asp:Button runat="server" Text="新增" OnClick="Unnamed_Click" />
    <asp:GridView runat="server" ID="GV1" AutoGenerateColumns="false" OnRowCancelingEdit="GV1_RowCancelingEdit" OnRowDeleting="GV1_RowDeleting" OnRowEditing="GV1_RowEditing" OnRowUpdating="GV1_RowUpdating">
        <Columns>
            <asp:BoundField DataField="Id" HeaderText="Id" ReadOnly="true" />
            <asp:BoundField DataField="Title" HeaderText="标题" />
            <asp:BoundField DataField="Content" HeaderText="内容" />
            <asp:BoundField DataField="Author" HeaderText="作者" />
            <asp:CommandField HeaderText="操作" ShowDeleteButton="true" ShowEditButton="true" ShowHeader="true" />
        </Columns>
    </asp:GridView>

</asp:Content>
