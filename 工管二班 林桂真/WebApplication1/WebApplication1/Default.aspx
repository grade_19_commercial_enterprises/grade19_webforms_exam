﻿<%@ Page Title="Home Page" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Default.aspx.cs" Inherits="WebApplication1._Default" %>

<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">
    <style>
        td {
            text-align:center;
            background-color:lightcyan;
        }
        th {
            text-align:center;
            width:140px;
            background-color:cadetblue;
        }
        body {
         height:1000px;
         width:530px;
         margin:0px auto;
         background-color:azure;
        }
    </style>
  <asp:GridView runat="server" ID="GridView" AutoGenerateColumns="false" OnRowCancelingEdit="GridView_RowCancelingEdit" OnRowDeleting="GridView_RowDeleting" OnRowEditing="GridView_RowEditing" OnRowUpdating="GridView_RowUpdating">
     
      <Columns>
          <asp:BoundField DataField="id" HeaderText="ID" />
          <asp:BoundField DataField="Title" HeaderText="标题 "/>
          <asp:BoundField DataField="Content" HeaderText=" 内容"/>
          <asp:BoundField DataField="Author" HeaderText=" 作者"/>
          <asp:CommandField ShowDeleteButton="true" ShowEditButton="true" ShowHeader="true" HeaderText="操作" />

      </Columns>
  </asp:GridView>
    <br />
    <asp:Button runat="server" Text="添加" OnClick="Unnamed_Click" />
</asp:Content>
